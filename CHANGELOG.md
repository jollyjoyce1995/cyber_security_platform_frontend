# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
	
### Added

### Changed

## [1.0.1] - 2017-03-07

### Added

- CHANGELOG added

### Changed

- Extended README containing information on installing the software
- Correct version in package.json

## [1.0.0] - 2017-02-23

### Added

- Initial release of the software, as used in the sIT Cyber Security Challenge 2016

[Unreleased]: https://gitlab.com/fhcw_itsecurity/cyber_security_platform_frontend/compare/compare/1.0.0...HEAD
[1.0.1]: https://gitlab.com/fhcw_itsecurity/cyber_security_platform_frontend/compare/compare/1.0.0...1.0.1
[1.0.0]: https://gitlab.com/fhcw_itsecurity/cyber_security_platform_frontend/compare/cb360903e50079f883b1172f97991e6b545eb2a2...1.0.0