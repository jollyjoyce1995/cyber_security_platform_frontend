angular.
module('cscFrontend.model_challenge', ['ngResource']).
factory('Challenge', function(config, $resource) {
        return $resource(config.apiUrl + '/challenges/:challengeId', {}, {
        });
    }
);