'use strict';

angular.module('cscFrontend.view_event', ['cscFrontend.model_event','cscFrontend.model_challengeset','cscFrontend.model_challenge','ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view_event/:eventId', {
            templateUrl: 'components/view_event/view_event.html',
            controller: 'ViewEventController',
            controllerAs: "eventController"
        });
    }])

    .controller('ViewEventController', function (Event, ChallengeSet, Challenge, ChallengeUserDetails, SessionService, $routeParams, $location, $q, $sessionStorage) {
            var self = this;
            this.dateNow = new Date().toISOString();
            this.selectChallenge = function(challengeId) {
                this.selectedChallenge = challengeId;
            }
            this.openChallenge = function() {
                angular.element('#startChallengeModal').modal('hide');
                angular.element('.modal-backdrop').remove();
                $location.path('/view_challenge/'+this.selectedChallenge);
            };
            self.challengeSets = [];
            self.event = Event.get({eventId: $routeParams.eventId}, function(event) {
                $sessionStorage.activeEvent = event;
                var tmpSet = [];
                angular.forEach(event.ChallengeSets, function(set) {
                    ChallengeSet.get({challengeSetId: set.ChallengeSetId}, function(resource) {
                        var tmpChallenges = [];
                        angular.forEach(resource.Challenges, function(challengeId) {
                            $q.all([
                                Challenge.get({challengeId: challengeId}).$promise,
                                ChallengeUserDetails.get({userId: SessionService.getUser().Id, challengeId: challengeId}).$promise
                            ]).then(function(data) {
                                var c = data[0];
                                var c_details = data[1];
                                c.newChallenge = typeof c_details.AccessDate === "undefined";
                                c.accessedChallenge = typeof c_details.AccessDate != "undefined" && typeof c_details.SolvedDate === "undefined";
                                c.solvedChallenge = typeof c_details.SolvedDate != "undefined";
                                tmpChallenges.push(c);
                            });
                        });
                        resource.Challenges = tmpChallenges;
                        tmpSet.push(
                            angular.merge(resource,set));
                    });
                });
                event.ChallengeSets = tmpSet;
            });
    });