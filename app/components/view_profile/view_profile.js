'use strict';

angular.module('cscFrontend.view_profile', ['ngRoute', 'cscFrontend.model_user', 'cscFrontend.service_session', 'cscFrontend.service_notification', 'ui.validate'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view_profile', {
            templateUrl: 'components/view_profile/view_profile.html',
            controller: 'ViewProfileController',
            requireAuth: true
        });
    }])

    .controller('ViewProfileController', function ($scope, $http, config, User, SessionService, NotificationService) {
        $scope.userData = User.authenticatedUser();
        $scope.$watch("profileForm.$dirty", function() {
            NotificationService.reset();
        })
        $scope.saveUserData = function(userData, avatar) {
            if(avatar) {
                $http.put(config.apiUrl + "/user", {
                    FirstName: userData.FirstName,
                    LastName: userData.LastName,
                    Avatar: avatar.deleteAvatar ? "" : "data:" + avatar.image.filetype + ";base64, " + avatar.image.base64
                }).then(function() {
                    NotificationService.success("Your information has been updated.");
                    User.authenticatedUser(function(savedUser) {
                        $scope.userData = savedUser;
                        SessionService.setUser(savedUser);
                    });
                }, function(response) {
                    NotificationService.danger("An error occurred, sorry.");
                });
            } else {
                $http.put(config.apiUrl + "/user", {
                    FirstName: userData.FirstName,
                    LastName: userData.LastName
                }).then(function() {
                    NotificationService.success("Your information has been updated.");
                    User.authenticatedUser(function(savedUser) {
                        $scope.userData = savedUser;
                        SessionService.setUser(savedUser);
                    });
                    avatar.deleteAvatar = false;
                }, function(response) {
                    NotificationService.danger("An error occurred, sorry.");
                });
            }
            };
        $scope.changePassword = function(data) {
            $http.post(config.apiUrl + "/user/password", {
                NewPassword: data.NewPassword,
                OldPassword: data.OldPassword
            }).then(function() {
                NotificationService.success("Your password has been changed.");
                angular.element('#passwordChangeModal').modal('hide');
            }, function(response) {
                NotificationService.danger("Your current password is not correct");
            });
        };
    });