'use strict';

angular.module('cscFrontend.view_eventList', ['ngRoute', 'cscFrontend.model_event'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view_eventList', {
            templateUrl: 'components/view_eventList/view_eventList.html',
            controller: 'ViewEventListController',
            controllerAs: "eventListController",
            requireAuth: true
        });
    }])

    .controller('ViewEventListController', function ($location, $scope, $sessionStorage, Event) {
        this.eventList = [];
        this.openEvent = function(eventId) {
            $sessionStorage.activeEvent = Event.get({eventId: eventId}, function(event) {
                $location.path('/view_event/' + eventId);
            });
        }
        this.openRanking = function(eventId) {
            $sessionStorage.activeEvent = Event.get({eventId: eventId}, function(event) {
                $location.path('/view_ranking/'+eventId);
            });
        }
        var self = this;
        Event.userEvents().$promise.then(function(eventIds) {
            eventIds.forEach(function(e) {
                self.eventList.push(Event.get({eventId: e}))
            });
        })
    });