Cyber Security Challenge Platform Frontend
=========================================

# Overview

This project contains the frontend for the FH Campus Wien Cyber Security Challenge Platform

# Contribution

Development follows the [git branching model](http://nvie.com/posts/a-successful-git-branching-model/) using the standard prefixes of [git flow](https://github.com/nvie/gitflow).

The format of the Changelog is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

# Usage

## Requirements

* Node Package Manager (npm) is required to setup an environment.
* Install instructions: [https://www.npmjs.com/package/npm]

## Running a test server
1. Clone the repository
2. Change into the main directory of the project
3. Install all required dependencies and run server using `npm start`
4. Open `http://localhost:8000` in a browser